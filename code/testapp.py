from flask import Flask, url_for, redirect

app = Flask(__name__)
app.debug = True

@app.route('/')
def start():
	path = url_for('static', filename='index.html')
	return redirect(path)

if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=True)
