#!/bin/bash
set -ex
cd /home/ec2-user
aws s3 cp s3://$CHEF_BUCKET/latest .
latest=`cat latest`.tgz
aws s3 cp s3://$CHEF_BUCKET/$latest .
tar -xzvf $latest
chef-solo -o role[web] -c Provisioning/Chef/solo.rb
rm -f latest
rm -f $latest
rm -rf Provisioning
